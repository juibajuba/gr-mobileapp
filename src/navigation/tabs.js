/**
 * Tabs Scenes
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React from 'react';
import { Scene } from 'react-native-router-flux';

// Consts and Libs
import { AppConfig } from '@constants/';
import { AppStyles, AppSizes } from '@theme/';

// Components
import { TabIcon } from '@ui/'; 

// Scenes
import Placeholder from '@components/general/Placeholder';
import Error from '@components/general/Error';
import StyleGuide from '@containers/StyleGuideView';
import Recipes from '@containers/recipes/Browse/BrowseContainer';
import RecipeView from '@containers/recipes/RecipeView';

const navbarPropsTabs = {
  ...AppConfig.navbarProps, 
  sceneStyle: {
    ...AppConfig.navbarProps.sceneStyle,
    paddingBottom: AppSizes.tabbarHeight,
  },
};

/* Routes ==================================================================== */
const scenes = (
  <Scene key={'tabBar'} tabs tabBarIconContainerStyle={AppStyles.tabbar} pressOpacity={0.95}>
    

    <Scene
      key={'timeline'}
      {...navbarPropsTabs}
      title={'หน้าแรก'}
      component={Placeholder}
      icon={props => TabIcon({
        ...props,
        icon: 'home',
        title: 'หน้าแรก'
      })}
    />

    <Scene
      key={'cate'}
      {...navbarPropsTabs}
      title={'หมวดหมู่'}
      component={Placeholder}
      icon={props => TabIcon({
        ...props,
        icon: 'explore',
        title: 'หมวดหมู่'
      })}
    />

<Scene
      key={'basket'}
      {...navbarPropsTabs}
      title={'ตะกร้าสินค้า'}
      component={Placeholder}
      icon={props => TabIcon({
        ...props,
        icon: 'list',
        title: 'ตะกร้าสินค้า'
      })}
    />

    <Scene
      key={'myproduct'}
      {...navbarPropsTabs}
      title={'สินค้าของฉัน'}
      component={Placeholder}
      icon={props => TabIcon({
        ...props,
        icon: 'face',
        title: 'สินค้าของฉัน'
      })}
    />
  </Scene>
);

export default scenes;
